package com.mycompany.mbravurajob;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hammer
 */
public class Job implements Serializable {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private Date date;
    private final String link;
    private final String title;
    private final String category;
    private final String area;

    public Job(String date, String link, String title, String category, String Area) {
        try {
            this.date = sdf.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(Job.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.link = link;
        this.title = title;
        this.category = category;
        this.area = Area;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getArea() {
        return area;
    }

    public String getCategory() {
        return category;
    }

    public Date getDate() {
        return date;
    }

}

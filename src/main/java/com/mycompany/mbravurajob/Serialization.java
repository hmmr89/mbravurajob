package com.mycompany.mbravurajob;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author Hammer
 */
public class Serialization {

    private static final String fileName = "jobs.ser";

    public boolean Save(Object o) {
        try {
            FileOutputStream fileOut = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(o);
            out.close();
            fileOut.close();
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public ArrayList<Job> load() {
        ArrayList<Job> jobList = null;
        try {
            FileInputStream fileIn = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            jobList = (ArrayList<Job>) in.readObject();
            in.close();
            fileIn.close();
            return jobList;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
}

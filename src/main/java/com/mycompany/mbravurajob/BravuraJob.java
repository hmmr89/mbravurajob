package com.mycompany.mbravurajob;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author Hammer
 */
public class BravuraJob {

    private static List<Job> jobList;
    private static DefaultTableModel tableModel;
    private static final JTextField searchField = new JTextField();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Serialization s = new Serialization();
        jobList = s.load();
        if (jobList == null) {
            UpdateJobList();
            s.Save(jobList);
        }
        ShowJobFrame();
    }

    private static void FilterJobs(String filter) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        RemoveTablemodelRows();
        for (Job j : jobList) {
            Object[] ob = {j.getTitle(), j.getCategory(), j.getArea(), dateFormat.format(j.getDate())};
            if (Search(ob, filter)) {
                tableModel.addRow(ob);
            }
        }
    }

    private static boolean Search(Object[] ob, String search) {
        String[] filters = search.split(" ");
        ArrayList<String> objectList = new ArrayList<>();

        for (Object o : ob) {
            if (o instanceof String) {
                objectList.add((String) o);
            }
            if (o instanceof Date) {
                objectList.add(((Date) o).toString());
            }
        }

        boolean result;
        for (String filter : filters) {
            result = false;
            for (String s : objectList) {
                if (s.toLowerCase().contains(filter.toLowerCase())) {
                    result = true;
                }
            }
            if (!result) {
                return false;
            }
        }
        return true;
    }

    private static void RemoveTablemodelRows() {
        if (tableModel.getRowCount() > 0) {
            for (int i = tableModel.getRowCount() - 1; i > -1; i--) {
                tableModel.removeRow(i);
            }
        }
    }

    private static void ShowJobFrame() throws HeadlessException {
        JFrame mainFrame = new JFrame("Job Search");

        JButton updateButton = CreateUpdateButton();
        JPanel searchPanel = CreateSearchPanel();

        JPanel searchUpdatePanel = new JPanel(new BorderLayout());
        searchUpdatePanel.add(searchPanel, BorderLayout.CENTER);
        searchUpdatePanel.add(updateButton, BorderLayout.LINE_END);

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(searchUpdatePanel, BorderLayout.PAGE_START);
        mainPanel.add(CreateJobPanel());

        mainFrame.add(mainPanel);
        mainFrame.pack();
        mainFrame.setDefaultCloseOperation(3);
        mainFrame.setVisible(true);
    }

    private static JButton CreateUpdateButton() {
        JButton updateButton = new JButton("Update");
        updateButton.addActionListener(e -> {
            UpdateJobList();
            new Serialization().Save(jobList);
            FilterJobs(searchField.getText());
        });
        return updateButton;
    }

    private static JPanel CreateSearchPanel() {
        JPanel searchPanel = new JPanel(new BorderLayout());
        searchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent de) {
                FilterJobs(searchField.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                FilterJobs(searchField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent de) {
            }
        });
        searchPanel.add(searchField);
        return searchPanel;
    }

    private static JPanel CreateJobPanel() {
        JScrollPane js = new JScrollPane(CreateJobTable());
        js.setVisible(true);
        js.setPreferredSize(new Dimension(800, 800));
        JPanel panel = new JPanel(new CardLayout());
        panel.add(js);
        return panel;
    }

    private static JTable CreateJobTable() {
        final String col[] = {"Title", "Category", "Area", "Date"};
        tableModel = new DefaultTableModel(col, 0) {
            @Override
            public boolean isCellEditable(int i, int i1) {
                return false;
            }
        };
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (Job j : jobList) {
            Object[] ob = {j.getTitle(), j.getCategory(), j.getArea(), dateFormat.format(j.getDate())};
            tableModel.addRow(ob);
        }
        JTable table = new JTable(tableModel);
        table.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
            }

            @Override
            public void mousePressed(MouseEvent me) {
                if (table.getSelectedRow() != -1) {
                    OpenLink();
                }
            }

            private void OpenLink() {
                String title = (String) table.getModel().getValueAt(table.getSelectedRow(), 0);
                for (Job j : jobList) {
                    if (j.getTitle().equals(title)) {
                        try {
                            Desktop.getDesktop().browse(new URI(j.getLink()));
                        } catch (Exception ex) {
                            Logger.getLogger(BravuraJob.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        break;
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        });
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        return table;
    }

    private static void UpdateJobList() {
        jobList = new ArrayList<>();
        WebDriver driver = new FirefoxDriver();
        driver.get("http://bravura.se/lediga-jobb/");
        List<WebElement> elements = driver.findElements(By.className("job-listing"));

        for (WebElement e : elements) {
            //System.out.println(e.+":"+e.getText());
            String date = e.findElement(By.className("date")).getText();
            String link = e.findElement(By.cssSelector("a[href*='lediga-jobb']")).getAttribute("href");
            String title = e.findElement(By.className("title")).getText();
            String category = e.findElement(By.className("category")).getText();
            String area = e.findElement(By.className("area")).getText();
            jobList.add(new Job(date, link, title, category, area));
        }
        driver.quit();
    }

}
